Results for kyber, 2 xterm /bin/true, 0 seq readers and 0 seq writers
Latency statistics:
         min         max         avg     std_dev     conf99%
       0.117       0.157       0.137   0.0282843     3.16615
Aggregated throughput:
         min         max         avg     std_dev     conf99%
         0.1         3.3     2.17667     1.80045     36.0189
Read throughput:
         min         max         avg     std_dev     conf99%
         0.1        3.29     2.16667     1.79205     35.8507
Write throughput:
         min         max         avg     std_dev     conf99%
           0        0.02        0.01        0.01    0.200054
