Results for mq-deadline, 2 xterm /bin/true, 10 seq readers and 0 seq writers
Latency statistics:
         min         max         avg     std_dev     conf99%
       0.718       0.747      0.7325   0.0205061     2.29546
Aggregated throughput:
         min         max         avg     std_dev     conf99%
        1407     1563.09     1493.89     66.2856     552.594
Read throughput:
         min         max         avg     std_dev     conf99%
     1406.97     1563.07     1493.88     66.2875     552.609
Write throughput:
         min         max         avg     std_dev     conf99%
           0        0.03       0.014   0.0134164    0.111847
