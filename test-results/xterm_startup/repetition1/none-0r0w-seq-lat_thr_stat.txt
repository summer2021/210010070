Results for none, 2 xterm /bin/true, 0 seq readers and 0 seq writers
Latency statistics:
         min         max         avg     std_dev     conf99%
       0.118       0.135      0.1265   0.0120208     1.34561
Aggregated throughput:
         min         max         avg     std_dev     conf99%
         0.1        3.38     2.24667     1.86004     37.2108
Read throughput:
         min         max         avg     std_dev     conf99%
         0.1        3.36     2.23667     1.85122     37.0346
Write throughput:
         min         max         avg     std_dev     conf99%
           0        0.02        0.01        0.01    0.200054
