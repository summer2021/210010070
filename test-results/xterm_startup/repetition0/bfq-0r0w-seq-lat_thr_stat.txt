Results for bfq, 2 xterm /bin/true, 0 seq readers and 0 seq writers
Latency statistics:
         min         max         avg     std_dev     conf99%
       0.119       0.155       0.137   0.0254558     2.84953
Aggregated throughput:
         min         max         avg     std_dev     conf99%
         0.1        3.18     2.09667     1.73125     34.6345
Read throughput:
         min         max         avg     std_dev     conf99%
         0.1        3.16     2.08333     1.71972     34.4037
Write throughput:
         min         max         avg     std_dev     conf99%
           0        0.02   0.0133333    0.011547    0.231003
