Results for bfq, 2 xterm /bin/true, 10 seq readers and 0 seq writers
Latency statistics:
         min         max         avg     std_dev     conf99%
       0.099         0.1      0.0995 0.000707107   0.0791537
Aggregated throughput:
         min         max         avg     std_dev     conf99%
     1258.82     1407.33     1324.36     75.7737     1515.89
Read throughput:
         min         max         avg     std_dev     conf99%
     1258.81     1407.33     1324.35     75.7804     1516.02
Write throughput:
         min         max         avg     std_dev     conf99%
           0        0.02        0.01        0.01    0.200054
