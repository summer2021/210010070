Results for kyber, 2 xterm /bin/true, 0 seq readers and 0 seq writers
Latency statistics:
         min         max         avg     std_dev     conf99%
       0.138       0.143      0.1405  0.00353553    0.395769
Aggregated throughput:
         min         max         avg     std_dev     conf99%
        0.13        3.06        2.06      1.6718      33.445
Read throughput:
         min         max         avg     std_dev     conf99%
        0.11        3.06     2.05333     1.68334      33.676
Write throughput:
         min         max         avg     std_dev     conf99%
           0        0.02  0.00666667    0.011547    0.231003
