Results for mq-deadline, 2 xterm /bin/true, 0 seq readers and 0 seq writers
Latency statistics:
         min         max         avg     std_dev     conf99%
       0.123       0.126      0.1245  0.00212132    0.237461
Aggregated throughput:
         min         max         avg     std_dev     conf99%
        0.09        3.03        2.03     1.68036     33.6163
Read throughput:
         min         max         avg     std_dev     conf99%
        0.09        3.01        2.02     1.67162     33.4414
Write throughput:
         min         max         avg     std_dev     conf99%
           0        0.02        0.01        0.01    0.200054
