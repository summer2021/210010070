Results for none, 2 xterm /bin/true, 5 seq readers and 5 seq writers
Latency statistics:
         min         max         avg     std_dev     conf99%
       0.206       0.423      0.3145    0.153442     17.1764
Aggregated throughput:
         min         max         avg     std_dev     conf99%
     1362.91     1586.85     1500.92     120.715     2414.95
Read throughput:
         min         max         avg     std_dev     conf99%
     1360.26     1581.41     1497.35     119.731     2395.26
Write throughput:
         min         max         avg     std_dev     conf99%
        2.64        5.44     3.57667      1.6137     32.2828
