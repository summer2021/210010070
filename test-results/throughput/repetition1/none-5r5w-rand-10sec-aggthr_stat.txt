Results for none, 5 rand readers and 5 rand writers
Aggregated throughput:
         min         max         avg     std_dev     conf99%
        94.8       150.6     134.632     26.6607     301.148
Read throughput:
         min         max         avg     std_dev     conf99%
       53.58       72.42       58.85      9.0886     102.661
Write throughput:
         min         max         avg     std_dev     conf99%
       22.38       95.05     75.7825     35.6495     402.682
