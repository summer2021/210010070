Results for mq-deadline, 10 seq readers and 0 seq writers
Aggregated throughput:
         min         max         avg     std_dev     conf99%
     1657.32     1790.95     1718.79     55.2792     624.412
Read throughput:
         min         max         avg     std_dev     conf99%
     1657.32     1790.93     1718.78     55.2705     624.313
Write throughput:
         min         max         avg     std_dev     conf99%
           0        0.02       0.005        0.01    0.112956
