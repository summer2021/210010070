Results for mq-deadline, 1 seq readers and 0 seq writers
Aggregated throughput:
         min         max         avg     std_dev     conf99%
     1264.75      1375.5     1328.38      47.312     534.418
Read throughput:
         min         max         avg     std_dev     conf99%
     1264.75      1375.5     1328.37     47.3129     534.428
Write throughput:
         min         max         avg     std_dev     conf99%
           0        0.03      0.0075       0.015    0.169434
