Results for none, 10 rand readers and 0 rand writers
Aggregated throughput:
         min         max         avg     std_dev     conf99%
       95.57       99.83      98.155     1.82562     20.6215
Read throughput:
         min         max         avg     std_dev     conf99%
       95.57       99.83       98.15     1.82318     20.5939
Write throughput:
         min         max         avg     std_dev     conf99%
           0        0.02       0.005        0.01    0.112956
