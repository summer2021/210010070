Results for mq-deadline, 5 rand readers and 5 rand writers
Aggregated throughput:
         min         max         avg     std_dev     conf99%
       86.97      114.27     100.012     14.5143     163.947
Read throughput:
         min         max         avg     std_dev     conf99%
       43.15        61.4     52.1325     10.0729      113.78
Write throughput:
         min         max         avg     std_dev     conf99%
       26.65       71.12       47.88     24.5527     277.337
