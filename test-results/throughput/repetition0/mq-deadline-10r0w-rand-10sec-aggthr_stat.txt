Results for mq-deadline, 10 rand readers and 0 rand writers
Aggregated throughput:
         min         max         avg     std_dev     conf99%
       88.08       90.43     89.1075     1.03297      11.668
Read throughput:
         min         max         avg     std_dev     conf99%
       88.06       90.43     89.1025     1.03963     11.7432
Write throughput:
         min         max         avg     std_dev     conf99%
           0        0.02       0.005        0.01    0.112956
