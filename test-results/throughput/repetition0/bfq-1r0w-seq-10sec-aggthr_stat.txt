Results for bfq, 1 seq readers and 0 seq writers
Aggregated throughput:
         min         max         avg     std_dev     conf99%
     1065.24     1194.64     1129.31     53.3336     602.435
Read throughput:
         min         max         avg     std_dev     conf99%
     1065.24     1194.62     1129.31     53.3254     602.343
Write throughput:
         min         max         avg     std_dev     conf99%
           0        0.02       0.005        0.01    0.112956
