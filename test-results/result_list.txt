throughput--10r-rand--mq-deadline pass 87.01 89.1075 88.0588 1.48316 MB/s
throughput--10r-rand--bfq pass 65.13 73.145 69.1375 5.66746 MB/s
throughput--10r-rand--kyber pass 93.0075 94.6575 93.8325 1.16673 MB/s
throughput--10r-rand--none pass 94.5475 98.155 96.3512 2.55089 MB/s
throughput--10r-seq--mq-deadline pass 1600.23 1718.79 1659.51 83.8346 MB/s
throughput--10r-seq--bfq pass 1168.78 1324.93 1246.86 110.415 MB/s
throughput--10r-seq--kyber pass 1693.87 1751.13 1722.5 40.4889 MB/s
throughput--10r-seq--none pass 1712.32 1733.12 1722.72 14.7078 MB/s
throughput--1r-seq--mq-deadline pass 1235.46 1328.38 1281.92 65.7044 MB/s
throughput--1r-seq--bfq pass 1129.31 1146.5 1137.9 12.1552 MB/s
throughput--1r-seq--kyber pass 1189.49 1245.95 1217.72 39.9232 MB/s
throughput--1r-seq--none pass 1186.1 1241.96 1214.03 39.499 MB/s
throughput--5r5w-rand--mq-deadline pass 100.012 110.877 105.445 7.68272 MB/s
throughput--5r5w-rand--bfq pass 57.4475 91.6325 74.54 24.1724 MB/s
throughput--5r5w-rand--kyber pass 101.92 129.918 115.919 19.7976 MB/s
throughput--5r5w-rand--none pass 134.632 144.212 139.422 6.77408 MB/s
throughput--5r5w-seq--mq-deadline pass 1179.15 1252.2 1215.68 51.6542 MB/s
throughput--5r5w-seq--bfq pass 1055.67 1076.85 1066.26 14.9765 MB/s
throughput--5r5w-seq--kyber pass 1515.04 1557.12 1536.08 29.7551 MB/s
throughput--5r5w-seq--none pass 1440.51 1474.87 1457.69 24.2962 MB/s
xterm_startup--0r-seq--mq-deadline pass 0.1245 0.1275 0.126 0.00212132 sec
xterm_startup--0r-seq--bfq pass 0.137 0.137 0.137 0 sec
xterm_startup--0r-seq--kyber pass 0.137 0.1405 0.13875 0.00247487 sec
xterm_startup--0r-seq--none pass 0.1265 0.142 0.13425 0.0109602 sec
xterm_startup--10r-seq--mq-deadline pass 0.5505 0.7325 0.6415 0.128693 sec
xterm_startup--10r-seq--bfq pass 0.0895 0.0995 0.0945 0.00707107 sec
xterm_startup--10r-seq--kyber pass 0.34 0.517 0.4285 0.125158 sec
xterm_startup--10r-seq--none pass 0.614 0.7095 0.66175 0.0675287 sec
xterm_startup--5r5w-seq--mq-deadline pass 0.2575 0.3375 0.2975 0.0565685 sec
xterm_startup--5r5w-seq--bfq pass 0.1225 0.124 0.12325 0.00106066 sec
xterm_startup--5r5w-seq--kyber pass 0.293 0.35 0.3215 0.0403051 sec
xterm_startup--5r5w-seq--none pass 0.3145 0.364 0.33925 0.0350018 sec
gnome_terminal_startup--0r-seq--mq-deadline pass 0.2275 0.2425 0.235 0.0106066 sec
gnome_terminal_startup--0r-seq--bfq pass 0.259 0.264 0.2615 0.00353553 sec
gnome_terminal_startup--0r-seq--kyber pass 0.2615 0.3815 0.3215 0.0848528 sec
gnome_terminal_startup--0r-seq--none pass 0.263 0.39 0.3265 0.0898026 sec
gnome_terminal_startup--10r-seq--mq-deadline pass 1.685 1.9825 1.83375 0.210364 sec
gnome_terminal_startup--10r-seq--bfq pass 3.1765 4.324 3.75025 0.811405 sec
gnome_terminal_startup--10r-seq--kyber pass 2.0775 2.252 2.16475 0.12339 sec
gnome_terminal_startup--10r-seq--none pass 1.6085 1.9885 1.7985 0.268701 sec
gnome_terminal_startup--5r5w-seq--mq-deadline pass 1.2875 1.421 1.35425 0.0943988 sec
gnome_terminal_startup--5r5w-seq--bfq pass 2.9665 3.046 3.00625 0.056215 sec
gnome_terminal_startup--5r5w-seq--kyber pass 1.057 1.1765 1.11675 0.0844993 sec
gnome_terminal_startup--5r5w-seq--none pass 0.975 1.099 1.037 0.0876812 sec
video_playing--0r-seq--mq-deadline pass 0 0 0 0 drop rate
video_playing--0r-seq--bfq pass 0 0 0 0 drop rate
video_playing--0r-seq--kyber pass 0 0 0 0 drop rate
video_playing--0r-seq--none pass 0 0 0 0 drop rate
video_playing--10r-seq--mq-deadline pass 49.3311 51.4584 50.3948 1.50423 drop rate
video_playing--10r-seq--bfq pass 0.501672 1.00334 0.752506 0.354733 drop rate
video_playing--10r-seq--kyber pass 33.612 42.7192 38.1656 6.43976 drop rate
video_playing--10r-seq--none pass 42.1405 49.8328 45.9866 5.43928 drop rate
video_playing--5r5w-seq--mq-deadline pass 6.52174 6.9398 6.73077 0.295613 drop rate
video_playing--5r5w-seq--bfq pass 0.083612 0.41806 0.250836 0.23649 drop rate
video_playing--5r5w-seq--kyber pass 10.1171 11.5385 10.8278 1.00508 drop rate
video_playing--5r5w-seq--none pass 8.69565 9.61538 9.15552 0.650347 drop rate
