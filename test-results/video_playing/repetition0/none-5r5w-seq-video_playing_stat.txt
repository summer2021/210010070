Results for none, 2 dd if=/home/lixiuxu/S/video_playing_vs_commands/../workfiles/smallfile of=/dev/null iflag=nocache bs=1M count=15, 5 seq readers and 5 seq writers
Frame drops:
         min         max         avg     std_dev     conf99%
          28          76          52     33.9411     3799.38
Frame drop rate:
         min         max         avg     std_dev     conf99%
     4.68227      12.709     8.69565     5.67577     635.348
Latency:
         min         max         avg     std_dev     conf99%
        0.16        0.32       0.216   0.0552167    0.245054
Aggregated throughput:
         min         max         avg     std_dev     conf99%
      710.08     1262.75     886.685      156.03     504.943
Read throughput:
         min         max         avg     std_dev     conf99%
      710.08     1257.32     882.426     156.556     506.647
Write throughput:
         min         max         avg     std_dev     conf99%
           0       12.43     4.25875     3.95193     12.7892
