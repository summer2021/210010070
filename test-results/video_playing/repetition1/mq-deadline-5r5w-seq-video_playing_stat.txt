Results for mq-deadline, 2 dd if=/home/lixiuxu/S/video_playing_vs_commands/../workfiles/smallfile of=/dev/null iflag=nocache bs=1M count=15, 5 seq readers and 5 seq writers
Frame drops:
         min         max         avg     std_dev     conf99%
          31          47          39     11.3137     1266.46
Frame drop rate:
         min         max         avg     std_dev     conf99%
     5.18395     7.85953     6.52174     1.89193     211.783
Latency:
         min         max         avg     std_dev     conf99%
        0.17        0.29       0.226   0.0478888    0.212532
Aggregated throughput:
         min         max         avg     std_dev     conf99%
      700.88     1281.76       867.2     173.695      562.11
Read throughput:
         min         max         avg     std_dev     conf99%
      700.88     1281.76     863.206     174.207     563.767
Write throughput:
         min         max         avg     std_dev     conf99%
           0       12.21     3.99375     4.14214     13.4048
