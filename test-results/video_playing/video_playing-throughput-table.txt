# video playing throughput
# First column: Workload
# Next columns: Aggregate throughput [MB/sec], or X if application did not
#               start up in 120 seconds (and a timeout fired)
# Reference case: none
# Reference-case meaning: 
#
# Workload           mq-deadline                 bfq               kyber                none
  0r-seq               4.53107             4.46393             4.45965             4.48893
  10r-seq              1088.94             683.466             1120.39             1206.78
  5r5w-seq             872.212             543.112             906.724              887.53
