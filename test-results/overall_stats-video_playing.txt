Results for mq-deadline, 2 dd if=/home/lixiuxu/S/video_playing_vs_commands/../workfiles/smallfile of=/dev/null iflag=nocache bs=1M count=15, 0 seq readers and 0 seq writers
2 repetitions
Frame drops:
         min         max         avg     std_dev     conf99%
           0           0           0           0           0
Frame drop rate:
         min         max         avg     std_dev     conf99%
           0           0           0           0           0
Latency:
         min         max         avg     std_dev     conf99%
       0.022       0.025      0.0235  0.00212132    0.237461
Aggregated throughput:
         min         max         avg     std_dev     conf99%
     4.52571     4.53643     4.53107  0.00758018    0.848528
Read throughput:
         min         max         avg     std_dev     conf99%
     4.50071     4.50786     4.50428  0.00505581    0.565949
Write throughput:
         min         max         avg     std_dev     conf99%
       0.025   0.0285714   0.0267857  0.00252536     0.28269
Results for bfq, 2 dd if=/home/lixiuxu/S/video_playing_vs_commands/../workfiles/smallfile of=/dev/null iflag=nocache bs=1M count=15, 0 seq readers and 0 seq writers
2 repetitions
Frame drops:
         min         max         avg     std_dev     conf99%
           0           0           0           0           0
Frame drop rate:
         min         max         avg     std_dev     conf99%
           0           0           0           0           0
Latency:
         min         max         avg     std_dev     conf99%
       0.032       0.037      0.0345  0.00353553    0.395769
Aggregated throughput:
         min         max         avg     std_dev     conf99%
     4.45929     4.46857     4.46393  0.00656195    0.734547
Read throughput:
         min         max         avg     std_dev     conf99%
     4.43786     4.44286     4.44036  0.00353553    0.395769
Write throughput:
         min         max         avg     std_dev     conf99%
   0.0214286   0.0257143   0.0235715  0.00303045    0.339229
Results for kyber, 2 dd if=/home/lixiuxu/S/video_playing_vs_commands/../workfiles/smallfile of=/dev/null iflag=nocache bs=1M count=15, 0 seq readers and 0 seq writers
2 repetitions
Frame drops:
         min         max         avg     std_dev     conf99%
           0           0           0           0           0
Frame drop rate:
         min         max         avg     std_dev     conf99%
           0           0           0           0           0
Latency:
         min         max         avg     std_dev     conf99%
       0.024       0.025      0.0245 0.000707107   0.0791537
Aggregated throughput:
         min         max         avg     std_dev     conf99%
     4.45786     4.46143     4.45965  0.00252437    0.282579
Read throughput:
         min         max         avg     std_dev     conf99%
     4.43643        4.44     4.43821  0.00252437    0.282579
Write throughput:
         min         max         avg     std_dev     conf99%
   0.0214286   0.0214286   0.0214286           0           0
Results for none, 2 dd if=/home/lixiuxu/S/video_playing_vs_commands/../workfiles/smallfile of=/dev/null iflag=nocache bs=1M count=15, 0 seq readers and 0 seq writers
2 repetitions
Frame drops:
         min         max         avg     std_dev     conf99%
           0           0           0           0           0
Frame drop rate:
         min         max         avg     std_dev     conf99%
           0           0           0           0           0
Latency:
         min         max         avg     std_dev     conf99%
       0.022       0.025      0.0235  0.00212132    0.237461
Aggregated throughput:
         min         max         avg     std_dev     conf99%
     4.48786        4.49     4.48893  0.00151321    0.169389
Read throughput:
         min         max         avg     std_dev     conf99%
     4.46571     4.46857     4.46714  0.00202233     0.22638
Write throughput:
         min         max         avg     std_dev     conf99%
   0.0214286   0.0221429   0.0217857 0.000505086   0.0565395
Results for mq-deadline, 2 dd if=/home/lixiuxu/S/video_playing_vs_commands/../workfiles/smallfile of=/dev/null iflag=nocache bs=1M count=15, 10 seq readers and 0 seq writers
2 repetitions
Frame drops:
         min         max         avg     std_dev     conf99%
         295         308       301.5     9.19239        1029
Frame drop rate:
         min         max         avg     std_dev     conf99%
     49.3311     51.4584     50.3948     1.50423     168.384
Latency:
         min         max         avg     std_dev     conf99%
    0.377143    0.430769    0.403956   0.0379193      4.2447
Aggregated throughput:
         min         max         avg     std_dev     conf99%
     1020.29      1157.6     1088.94     97.0928     10868.6
Read throughput:
         min         max         avg     std_dev     conf99%
     1020.28     1157.59     1088.93     97.0928     10868.6
Write throughput:
         min         max         avg     std_dev     conf99%
   0.0145455   0.0145833   0.0145644 2.67286e-05  0.00299201
Results for bfq, 2 dd if=/home/lixiuxu/S/video_playing_vs_commands/../workfiles/smallfile of=/dev/null iflag=nocache bs=1M count=15, 10 seq readers and 0 seq writers
2 repetitions
Frame drops:
         min         max         avg     std_dev     conf99%
           3           6         4.5     2.12132     237.461
Frame drop rate:
         min         max         avg     std_dev     conf99%
    0.501672     1.00334    0.752506    0.354733     39.7089
Latency:
         min         max         avg     std_dev     conf99%
       0.089       0.094      0.0915  0.00353553    0.395769
Aggregated throughput:
         min         max         avg     std_dev     conf99%
     680.699     686.233     683.466     3.91313     438.037
Read throughput:
         min         max         avg     std_dev     conf99%
      680.68     686.211     683.446     3.91101     437.799
Write throughput:
         min         max         avg     std_dev     conf99%
   0.0192857   0.0214286   0.0203571  0.00151526    0.169619
Results for kyber, 2 dd if=/home/lixiuxu/S/video_playing_vs_commands/../workfiles/smallfile of=/dev/null iflag=nocache bs=1M count=15, 10 seq readers and 0 seq writers
2 repetitions
Frame drops:
         min         max         avg     std_dev     conf99%
         201         256       228.5     38.8909     4353.46
Frame drop rate:
         min         max         avg     std_dev     conf99%
      33.612     42.7192     38.1656     6.43976     720.869
Latency:
         min         max         avg     std_dev     conf99%
    0.375833    0.456923    0.416378   0.0573393     6.41858
Aggregated throughput:
         min         max         avg     std_dev     conf99%
     1100.89     1139.88     1120.39     27.5701      3086.2
Read throughput:
         min         max         avg     std_dev     conf99%
     1100.88     1139.87     1120.38     27.5701      3086.2
Write throughput:
         min         max         avg     std_dev     conf99%
   0.0143478      0.0175   0.0159239  0.00222894    0.249508
Results for none, 2 dd if=/home/lixiuxu/S/video_playing_vs_commands/../workfiles/smallfile of=/dev/null iflag=nocache bs=1M count=15, 10 seq readers and 0 seq writers
2 repetitions
Frame drops:
         min         max         avg     std_dev     conf99%
         252         298         275     32.5269     3641.07
Frame drop rate:
         min         max         avg     std_dev     conf99%
     42.1405     49.8328     45.9866     5.43928     608.874
Latency:
         min         max         avg     std_dev     conf99%
    0.363571    0.384615    0.374093   0.0148804     1.66571
Aggregated throughput:
         min         max         avg     std_dev     conf99%
     1190.68     1222.88     1206.78     22.7688     2548.75
Read throughput:
         min         max         avg     std_dev     conf99%
     1190.67     1222.86     1206.76     22.7618     2547.96
Write throughput:
         min         max         avg     std_dev     conf99%
       0.016   0.0165217   0.0162609 0.000368898   0.0412945
Results for mq-deadline, 2 dd if=/home/lixiuxu/S/video_playing_vs_commands/../workfiles/smallfile of=/dev/null iflag=nocache bs=1M count=15, 5 seq readers and 5 seq writers
2 repetitions
Frame drops:
         min         max         avg     std_dev     conf99%
          39        41.5       40.25     1.76777     197.884
Frame drop rate:
         min         max         avg     std_dev     conf99%
     6.52174      6.9398     6.73077    0.295613      33.091
Latency:
         min         max         avg     std_dev     conf99%
       0.226       0.236       0.231  0.00707107    0.791537
Aggregated throughput:
         min         max         avg     std_dev     conf99%
       867.2     877.224     872.212     7.08804     793.437
Read throughput:
         min         max         avg     std_dev     conf99%
     863.206     872.976     868.091     6.90843     773.332
Write throughput:
         min         max         avg     std_dev     conf99%
     3.99375      4.2475     4.12062    0.179428     20.0853
Results for bfq, 2 dd if=/home/lixiuxu/S/video_playing_vs_commands/../workfiles/smallfile of=/dev/null iflag=nocache bs=1M count=15, 5 seq readers and 5 seq writers
2 repetitions
Frame drops:
         min         max         avg     std_dev     conf99%
         0.5         2.5         1.5     1.41421     158.307
Frame drop rate:
         min         max         avg     std_dev     conf99%
    0.083612     0.41806    0.250836     0.23649     26.4728
Latency:
         min         max         avg     std_dev     conf99%
       0.104       0.107      0.1055  0.00212132    0.237461
Aggregated throughput:
         min         max         avg     std_dev     conf99%
     442.885      643.34     543.112     141.743     15866.8
Read throughput:
         min         max         avg     std_dev     conf99%
     438.647     639.184     538.915     141.801     15873.3
Write throughput:
         min         max         avg     std_dev     conf99%
     4.15571       4.238     4.19686   0.0581878     6.51356
Results for kyber, 2 dd if=/home/lixiuxu/S/video_playing_vs_commands/../workfiles/smallfile of=/dev/null iflag=nocache bs=1M count=15, 5 seq readers and 5 seq writers
2 repetitions
Frame drops:
         min         max         avg     std_dev     conf99%
        60.5          69       64.75     6.01041     672.807
Frame drop rate:
         min         max         avg     std_dev     conf99%
     10.1171     11.5385     10.8278     1.00508     112.509
Latency:
         min         max         avg     std_dev     conf99%
    0.230909       0.234    0.232455  0.00218567    0.244664
Aggregated throughput:
         min         max         avg     std_dev     conf99%
     876.821     936.627     906.724     42.2892     4733.87
Read throughput:
         min         max         avg     std_dev     conf99%
     872.572     932.426     902.499     42.3232     4737.67
Write throughput:
         min         max         avg     std_dev     conf99%
     4.20111     4.24875     4.22493   0.0336866     3.77088
Results for none, 2 dd if=/home/lixiuxu/S/video_playing_vs_commands/../workfiles/smallfile of=/dev/null iflag=nocache bs=1M count=15, 5 seq readers and 5 seq writers
2 repetitions
Frame drops:
         min         max         avg     std_dev     conf99%
          52        57.5       54.75     3.88909     435.346
Frame drop rate:
         min         max         avg     std_dev     conf99%
     8.69565     9.61538     9.15552    0.650347     72.8001
Latency:
         min         max         avg     std_dev     conf99%
       0.216    0.253636    0.234818   0.0266127     2.97903
Aggregated throughput:
         min         max         avg     std_dev     conf99%
     886.685     888.376      887.53     1.19572     133.849
Read throughput:
         min         max         avg     std_dev     conf99%
     882.426     884.382     883.404      1.3831     154.825
Write throughput:
         min         max         avg     std_dev     conf99%
     3.99412     4.25875     4.12643    0.187122     20.9465
