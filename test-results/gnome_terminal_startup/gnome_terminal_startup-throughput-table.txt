# gnome terminal startup throughput
# First column: Workload
# Next columns: Aggregate throughput [MB/sec], or X if application did not
#               start up in 120 seconds (and a timeout fired)
# Reference case: none
# Reference-case meaning: 
#
# Workload           mq-deadline                 bfq               kyber                none
  0r-seq               6.67835               2.605                3.19                4.12
  10r-seq              1585.05             1235.56             1675.49             1641.68
  5r5w-seq             1440.15             1069.78             1499.07              1571.4
