Results for bfq, 2 gnome-terminal -e /bin/true, 5 seq readers and 5 seq writers
Latency statistics:
         min         max         avg     std_dev     conf99%
       2.422       3.511      2.9665    0.770039     86.1984
Aggregated throughput:
         min         max         avg     std_dev     conf99%
      966.22     1131.09     1058.69     68.5194     773.967
Read throughput:
         min         max         avg     std_dev     conf99%
      960.91     1128.43      1054.7     69.6775     787.049
Write throughput:
         min         max         avg     std_dev     conf99%
        2.65        5.31        3.98     1.52999     17.2821
