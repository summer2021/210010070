Results for bfq, 2 gnome-terminal -e /bin/true, 0 seq readers and 0 seq writers
Latency statistics:
         min         max         avg     std_dev     conf99%
       0.257       0.261       0.259  0.00282843    0.316615
Aggregated throughput:
         min         max         avg     std_dev     conf99%
        0.13        3.87        2.56     2.10658     42.1432
Read throughput:
         min         max         avg     std_dev     conf99%
         0.1        3.87        2.55     2.12389     42.4893
Write throughput:
         min         max         avg     std_dev     conf99%
           0        0.03        0.01   0.0173205    0.346504
