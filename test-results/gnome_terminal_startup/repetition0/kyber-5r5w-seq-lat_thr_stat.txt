Results for kyber, 2 gnome-terminal -e /bin/true, 5 seq readers and 5 seq writers
Latency statistics:
         min         max         avg     std_dev     conf99%
        0.81       1.304       1.057    0.349311     39.1019
Aggregated throughput:
         min         max         avg     std_dev     conf99%
     1450.85     1593.97     1510.56     62.5889     706.978
Read throughput:
         min         max         avg     std_dev     conf99%
     1445.48     1591.35     1506.56      63.958     722.444
Write throughput:
         min         max         avg     std_dev     conf99%
        2.62        5.37      3.9975     1.57908     17.8366
