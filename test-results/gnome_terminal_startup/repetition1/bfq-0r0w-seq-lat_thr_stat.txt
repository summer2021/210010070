Results for bfq, 2 gnome-terminal -e /bin/true, 0 seq readers and 0 seq writers
Latency statistics:
         min         max         avg     std_dev     conf99%
        0.26       0.268       0.264  0.00565685     0.63323
Aggregated throughput:
         min         max         avg     std_dev     conf99%
        0.05        4.13        2.65     2.25885     45.1893
Read throughput:
         min         max         avg     std_dev     conf99%
        0.05        4.11        2.64     2.24982     45.0087
Write throughput:
         min         max         avg     std_dev     conf99%
           0        0.02        0.01        0.01    0.200054
