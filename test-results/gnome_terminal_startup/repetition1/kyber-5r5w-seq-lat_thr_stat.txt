Results for kyber, 2 gnome-terminal -e /bin/true, 5 seq readers and 5 seq writers
Latency statistics:
         min         max         avg     std_dev     conf99%
       0.737       1.616      1.1765    0.621547     69.5761
Aggregated throughput:
         min         max         avg     std_dev     conf99%
     1330.02     1604.99     1487.59      115.54     1305.09
Read throughput:
         min         max         avg     std_dev     conf99%
     1324.71     1599.67     1483.61     115.856     1308.66
Write throughput:
         min         max         avg     std_dev     conf99%
        2.64        5.32      3.9775     1.54442     17.4451
