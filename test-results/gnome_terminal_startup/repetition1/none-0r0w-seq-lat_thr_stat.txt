Results for none, 2 gnome-terminal -e /bin/true, 0 seq readers and 0 seq writers
Latency statistics:
         min         max         avg     std_dev     conf99%
        0.26       0.266       0.263  0.00424264    0.474922
Aggregated throughput:
         min         max         avg     std_dev     conf99%
        0.04        5.36        3.54      3.0319     60.6544
Read throughput:
         min         max         avg     std_dev     conf99%
        0.04        5.34        3.53     3.02313      60.479
Write throughput:
         min         max         avg     std_dev     conf99%
           0        0.02        0.01        0.01    0.200054
