Results for mq-deadline, 2 gnome-terminal -e /bin/true, 0 seq readers and 0 seq writers
2 repetitions
Latency statistics:
         min         max         avg     std_dev     conf99%
      0.2275      0.2425       0.235   0.0106066     1.18731
Aggregated throughput:
         min         max         avg     std_dev     conf99%
         2.5     10.8567     6.67835     5.90908     661.464
Read throughput:
         min         max         avg     std_dev     conf99%
        2.49     10.4767     6.48335     5.64745     632.177
Write throughput:
         min         max         avg     std_dev     conf99%
        0.01        0.38       0.195     0.26163     29.2869
Results for bfq, 2 gnome-terminal -e /bin/true, 0 seq readers and 0 seq writers
2 repetitions
Latency statistics:
         min         max         avg     std_dev     conf99%
       0.259       0.264      0.2615  0.00353553    0.395769
Aggregated throughput:
         min         max         avg     std_dev     conf99%
        2.56        2.65       2.605   0.0636396     7.12384
Read throughput:
         min         max         avg     std_dev     conf99%
        2.55        2.64       2.595   0.0636396     7.12384
Write throughput:
         min         max         avg     std_dev     conf99%
        0.01        0.01        0.01           0           0
Results for kyber, 2 gnome-terminal -e /bin/true, 0 seq readers and 0 seq writers
2 repetitions
Latency statistics:
         min         max         avg     std_dev     conf99%
      0.2615      0.3815      0.3215   0.0848528     9.49845
Aggregated throughput:
         min         max         avg     std_dev     conf99%
        2.57        3.81        3.19    0.876812     98.1506
Read throughput:
         min         max         avg     std_dev     conf99%
        2.56        3.77       3.165    0.855599      95.776
Write throughput:
         min         max         avg     std_dev     conf99%
        0.01        0.04       0.025   0.0212132     2.37461
Results for none, 2 gnome-terminal -e /bin/true, 0 seq readers and 0 seq writers
2 repetitions
Latency statistics:
         min         max         avg     std_dev     conf99%
       0.263        0.39      0.3265   0.0898026     10.0525
Aggregated throughput:
         min         max         avg     std_dev     conf99%
        3.54         4.7        4.12    0.820244     91.8183
Read throughput:
         min         max         avg     std_dev     conf99%
        3.53        4.69        4.11    0.820244     91.8183
Write throughput:
         min         max         avg     std_dev     conf99%
        0.01        0.01        0.01           0           0
Results for mq-deadline, 2 gnome-terminal -e /bin/true, 10 seq readers and 0 seq writers
2 repetitions
Latency statistics:
         min         max         avg     std_dev     conf99%
       1.685      1.9825     1.83375    0.210364     23.5482
Aggregated throughput:
         min         max         avg     std_dev     conf99%
     1560.37     1609.72     1585.05     34.8957     3906.24
Read throughput:
         min         max         avg     std_dev     conf99%
     1560.36     1609.72     1585.04     34.9028     3907.03
Write throughput:
         min         max         avg     std_dev     conf99%
       0.005       0.005       0.005           0           0
Results for bfq, 2 gnome-terminal -e /bin/true, 10 seq readers and 0 seq writers
2 repetitions
Latency statistics:
         min         max         avg     std_dev     conf99%
      3.1765       4.324     3.75025    0.811405     90.8289
Aggregated throughput:
         min         max         avg     std_dev     conf99%
     1231.59     1239.53     1235.56     5.61443     628.481
Read throughput:
         min         max         avg     std_dev     conf99%
     1231.58     1239.52     1235.55     5.61443     628.481
Write throughput:
         min         max         avg     std_dev     conf99%
      0.0075      0.0125        0.01  0.00353553    0.395769
Results for kyber, 2 gnome-terminal -e /bin/true, 10 seq readers and 0 seq writers
2 repetitions
Latency statistics:
         min         max         avg     std_dev     conf99%
      2.0775       2.252     2.16475     0.12339     13.8123
Aggregated throughput:
         min         max         avg     std_dev     conf99%
     1653.33     1697.66     1675.49      31.346     3508.89
Read throughput:
         min         max         avg     std_dev     conf99%
     1653.32     1697.65     1675.49      31.346     3508.89
Write throughput:
         min         max         avg     std_dev     conf99%
       0.005       0.005       0.005           0           0
Results for none, 2 gnome-terminal -e /bin/true, 10 seq readers and 0 seq writers
2 repetitions
Latency statistics:
         min         max         avg     std_dev     conf99%
      1.6085      1.9885      1.7985    0.268701     30.0784
Aggregated throughput:
         min         max         avg     std_dev     conf99%
     1628.75     1654.61     1641.68     18.2858     2046.92
Read throughput:
         min         max         avg     std_dev     conf99%
     1628.74      1654.6     1641.67     18.2858     2046.92
Write throughput:
         min         max         avg     std_dev     conf99%
       0.006       0.008       0.007  0.00141421    0.158307
Results for mq-deadline, 2 gnome-terminal -e /bin/true, 5 seq readers and 5 seq writers
2 repetitions
Latency statistics:
         min         max         avg     std_dev     conf99%
      1.2875       1.421     1.35425   0.0943988      10.567
Aggregated throughput:
         min         max         avg     std_dev     conf99%
      1423.1     1457.21     1440.15     24.1194     2699.93
Read throughput:
         min         max         avg     std_dev     conf99%
     1419.12     1453.22     1436.17     24.1123     2699.14
Write throughput:
         min         max         avg     std_dev     conf99%
       3.975       3.985        3.98  0.00707107    0.791537
Results for bfq, 2 gnome-terminal -e /bin/true, 5 seq readers and 5 seq writers
2 repetitions
Latency statistics:
         min         max         avg     std_dev     conf99%
      2.9665       3.046     3.00625    0.056215     6.29272
Aggregated throughput:
         min         max         avg     std_dev     conf99%
     1058.69     1080.86     1069.78     15.6766     1754.84
Read throughput:
         min         max         avg     std_dev     conf99%
      1054.7     1076.88     1065.79     15.6836     1755.63
Write throughput:
         min         max         avg     std_dev     conf99%
        3.98        3.98        3.98           0           0
Results for kyber, 2 gnome-terminal -e /bin/true, 5 seq readers and 5 seq writers
2 repetitions
Latency statistics:
         min         max         avg     std_dev     conf99%
       1.057      1.1765     1.11675   0.0844993     9.45887
Aggregated throughput:
         min         max         avg     std_dev     conf99%
     1487.59     1510.56     1499.07     16.2422     1818.16
Read throughput:
         min         max         avg     std_dev     conf99%
     1483.61     1506.56     1495.09     16.2281     1816.58
Write throughput:
         min         max         avg     std_dev     conf99%
      3.9775      3.9975      3.9875   0.0141421     1.58307
Results for none, 2 gnome-terminal -e /bin/true, 5 seq readers and 5 seq writers
2 repetitions
Latency statistics:
         min         max         avg     std_dev     conf99%
       0.975       1.099       1.037   0.0876812     9.81506
Aggregated throughput:
         min         max         avg     std_dev     conf99%
     1563.09     1579.71      1571.4     11.7521     1315.54
Read throughput:
         min         max         avg     std_dev     conf99%
     1559.07     1575.74      1567.4     11.7875     1319.49
Write throughput:
         min         max         avg     std_dev     conf99%
        3.97        4.01        3.99   0.0282843     3.16615
